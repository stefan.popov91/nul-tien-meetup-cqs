﻿using FluentValidation;
using NulTienMeetupCqs.Starting.MediatR;
using System.Linq;

namespace NulTienMeetupCqs.Mediator.Orders.Commands
{
    public class ChargeOrderCommandValidators : AbstractValidator<ChargeOrderCommand>
    {
        public ChargeOrderCommandValidators()
        {
            var repositories = new Repositories();
            RuleFor(x => x).Must(x => repositories.Orders.Single(o => o.Id == x.Id).TotalPrice <= x.PaidAmount);


        }
    }
}
