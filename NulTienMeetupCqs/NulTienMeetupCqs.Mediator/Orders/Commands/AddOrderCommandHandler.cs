﻿using MediatR;
using NulTienMeetupCqs.Starting.MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NulTienMeetupCqs.Mediator.Orders.Commands
{
    public class AddOrderCommandHandler : IRequestHandler<AddOrderCommand, NoResult>
    {
        private readonly Repositories _repositories;

        public AddOrderCommandHandler()
        {
            _repositories = new Repositories();
        }

        public Task<NoResult> Handle(AddOrderCommand request, CancellationToken cancellationToken)
        {
            var order = new Order
            {
                Id = _repositories.Orders.Max(x => x.Id) + 1,
                OrderItems = request.OrderItems,
            };
            _repositories.Orders.Add(order);

            return Task.FromResult(new NoResult());
        }
    }
}
