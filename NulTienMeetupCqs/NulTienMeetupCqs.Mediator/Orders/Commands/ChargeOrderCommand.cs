﻿using MediatR;

namespace NulTienMeetupCqs.Mediator.Orders.Commands
{
    public class ChargeOrderCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int PaidAmount { get; set; }
    }
}
