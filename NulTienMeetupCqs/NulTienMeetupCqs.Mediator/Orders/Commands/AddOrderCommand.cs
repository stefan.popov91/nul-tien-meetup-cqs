﻿using MediatR;
using NulTienMeetupCqs.Starting.MediatR;
using System.Collections.Generic;

namespace NulTienMeetupCqs.Mediator.Orders.Commands
{
    public class AddOrderCommand : IRequest<NoResult>
    {
        public IList<OrderItem> OrderItems { get; set; }
    }
}
