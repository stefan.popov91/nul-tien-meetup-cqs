﻿using MediatR;
using NulTienMeetupCqs.Starting.MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NulTienMeetupCqs.Mediator.Orders.Commands
{
    public class UpdateOrderCommandHandler : IRequestHandler<UpdateOrderCommand, NoResult>
    {
        private readonly Repositories _repositories;

        public UpdateOrderCommandHandler()
        {
            _repositories = new Repositories();
        }

        public Task<NoResult> Handle(UpdateOrderCommand request, CancellationToken cancellationToken)
        {
            var order = _repositories.Orders.Single(x => x.Id == request.Id);
            order.OrderItems.ToList().AddRange(request.OrderItems);

            return Task.FromResult(new NoResult());
        }
    }
}
