﻿using MediatR;
using NulTienMeetupCqs.Starting.MediatR;
using System.Collections.Generic;

namespace NulTienMeetupCqs.Mediator.Orders.Commands
{
    public class UpdateOrderCommand : IRequest<NoResult>
    {
        public int? Id { get; set; }
        public IList<OrderItem> OrderItems { get; set; }
    }
}
