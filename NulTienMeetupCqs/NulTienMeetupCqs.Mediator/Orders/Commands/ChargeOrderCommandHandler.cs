﻿using MediatR;
using NulTienMeetupCqs.Starting.MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NulTienMeetupCqs.Mediator.Orders.Commands
{
    public class ChargeOrderCommandHandler : IRequestHandler<ChargeOrderCommand, int>
    {
        private readonly Repositories _repositories;

        public ChargeOrderCommandHandler()
        {
            _repositories = new Repositories();
        }

        public Task<int> Handle(ChargeOrderCommand request, CancellationToken cancellationToken)
        {
            var order = _repositories.Orders.Single(x => x.Id == request.Id);
            order.Charged = true;
            return Task.FromResult(request.PaidAmount - order.TotalPrice);
        }
    }
}
