﻿using System.Threading.Tasks;

namespace NulTienMeetupCqs.Cqs.Core.Queries
{
    public interface IDomainQueryAsyncHandler<in TQuery, TResult> where TQuery : IDomainQuery<TResult>
    {
        Task<TResult> ExecuteAsync(TQuery query);
    }
}
