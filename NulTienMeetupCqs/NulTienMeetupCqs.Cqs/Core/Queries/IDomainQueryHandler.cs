﻿namespace NulTienMeetupCqs.Cqs.Core.Queries
{
    public interface IDomainQueryHandler<in TQuery, TResult> where TQuery : IDomainQuery<TResult>
    {
        TResult Execute(TQuery query);
    }
}
