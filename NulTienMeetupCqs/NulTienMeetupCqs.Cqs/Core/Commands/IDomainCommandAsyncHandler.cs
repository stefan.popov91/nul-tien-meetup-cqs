﻿using System.Threading.Tasks;

namespace NulTienMeetupCqs.Cqs.Core.Commands
{
   public interface IDomainCommandAsyncHandler<in TCommand> where TCommand : IDomainCommand
    {
        Task ExecuteAsync(TCommand command);
    }
}
