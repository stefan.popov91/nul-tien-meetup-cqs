﻿namespace NulTienMeetupCqs.Cqs.Core.Commands
{
    public interface IDomainCommandHandler<TCommand> where TCommand : IDomainCommand
    {
        void Execute(TCommand command);
    }
}
