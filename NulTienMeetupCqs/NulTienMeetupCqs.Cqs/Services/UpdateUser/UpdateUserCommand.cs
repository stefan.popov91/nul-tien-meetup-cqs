﻿using NulTienMeetupCqs.Cqs.Core.Commands;
using NulTienMeetupCqs.Cqs.Models;

namespace NulTienMeetupCqs.Cqs.Services.UpdateUser
{
   public class UpdateUserCommand : IDomainCommand
    {
        public User User { get; }

        public UpdateUserCommand(User user)
        {
            User = user;
        }
    }
}