﻿using NulTienMeetupCqs.Cqs.Core.Commands;
using NulTienMeetupCqs.Cqs.Data.Repositories;

namespace NulTienMeetupCqs.Cqs.Services.UpdateUser
{
    public class UpdateUserCommandHandler : IDomainCommandHandler<UpdateUserCommand>
    {
        private readonly UserRepository _userRepository;
        public UpdateUserCommandHandler()
        {
            _userRepository = new UserRepository();
        }

        public void Execute(UpdateUserCommand command)
        {
            if (!command.User.IsValid())
                return;

            var existingUser = _userRepository.FindById(command.User.Id);
            existingUser.UpdateFrom(command.User);
        }
    }
}
