﻿using System.Linq;
using NulTienMeetupCqs.Cqs.Core.Queries;
using NulTienMeetupCqs.Cqs.Data.Repositories;

namespace NulTienMeetupCqs.Cqs.Services.FindAllUsersByLastName
{
    public class FindAllActiveUsersByLastNameQueryHandler : IDomainQueryHandler<FindAllActiveUsersByLastNameQuery, FindAllActiveUsersByLastNameQueryResult>
    {
        private readonly UserRepository _userRepository;
        public FindAllActiveUsersByLastNameQueryHandler()
        {
            _userRepository = new UserRepository();
        }

        public FindAllActiveUsersByLastNameQueryResult Execute(FindAllActiveUsersByLastNameQuery query)
        {
            var users = _userRepository.GetAll().Where(u => u.LastName == query.LastName && u.IsActivated).ToList();
            return new FindAllActiveUsersByLastNameQueryResult(users);
        }
    }
}
