﻿using NulTienMeetupCqs.Cqs.Core.Queries;

namespace NulTienMeetupCqs.Cqs.Services.FindAllUsersByLastName
{
  public  class FindAllActiveUsersByLastNameQuery : IDomainQuery<FindAllActiveUsersByLastNameQueryResult>
    {
       public string LastName { get; }

        public FindAllActiveUsersByLastNameQuery(string lastName)
        {
            LastName = lastName;
        }
    }
}
