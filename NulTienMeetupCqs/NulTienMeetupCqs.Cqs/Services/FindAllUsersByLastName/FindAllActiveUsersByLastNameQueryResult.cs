﻿using System.Collections.Generic;
using NulTienMeetupCqs.Cqs.Models;

namespace NulTienMeetupCqs.Cqs.Services.FindAllUsersByLastName
{
   public class FindAllActiveUsersByLastNameQueryResult
    {
        public IList<User> Users { get; }

        public FindAllActiveUsersByLastNameQueryResult(IList<User> users)
        {
            Users = users;
        }

    }
}
