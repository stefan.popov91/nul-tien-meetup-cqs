﻿using System.Linq;
using NulTienMeetupCqs.Cqs.Core.Commands;
using NulTienMeetupCqs.Cqs.Data.Repositories;
using NulTienMeetupCqs.Cqs.Models;

namespace NulTienMeetupCqs.Cqs.Services.AddUser
{
    public class AddUserCommandHandler : IDomainCommandHandler<AddUserCommand>
    {
        private readonly UserRepository _userRepository;
        public AddUserCommandHandler()
        {
            _userRepository = new UserRepository();
        }

        public void Execute(AddUserCommand command)
        {
            if (UserWithSameUserNameAlreadyExists(command.User))
                return;

            if (!command.User.IsValid())
                return;

            _userRepository.Add(command.User);
        }

        private bool UserWithSameUserNameAlreadyExists(User user)
        {
            var existingUser = _userRepository.GetAll().FirstOrDefault(u => u.UserName == user.UserName);
            return existingUser != null;
        }
    }
}
