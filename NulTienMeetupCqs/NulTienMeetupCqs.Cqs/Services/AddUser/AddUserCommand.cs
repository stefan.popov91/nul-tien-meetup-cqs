﻿using NulTienMeetupCqs.Cqs.Core.Commands;
using NulTienMeetupCqs.Cqs.Models;

namespace NulTienMeetupCqs.Cqs.Services.AddUser
{
   public class AddUserCommand : IDomainCommand
    {
        public User User { get; }

        public AddUserCommand(User user)
        {
            User = user;
        }
    }
}