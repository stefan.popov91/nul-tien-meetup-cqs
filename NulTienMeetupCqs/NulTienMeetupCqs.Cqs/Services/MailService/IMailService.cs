﻿using NulTienMeetupCqs.Cqs.Models;

namespace NulTienMeetupCqs.Cqs.Services.MailService
{
    public interface IMailService
    {
        void SendUserActivation(User user);
    }
}
