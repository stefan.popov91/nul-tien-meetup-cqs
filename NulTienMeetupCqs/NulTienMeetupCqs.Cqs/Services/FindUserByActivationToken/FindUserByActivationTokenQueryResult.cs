﻿using NulTienMeetupCqs.Cqs.Models;

namespace NulTienMeetupCqs.Cqs.Services.FindUserByActivationToken
{
  public  class FindUserByActivationTokenQueryResult
    {
        public User User { get; }

        public FindUserByActivationTokenQueryResult(User user)
        {
            User = user;
        }
    }
}
