﻿using NulTienMeetupCqs.Cqs.Core.Queries;

namespace NulTienMeetupCqs.Cqs.Services.FindUserByActivationToken
{
    public class FindUserByActivationTokenQuery : IDomainQuery<FindUserByActivationTokenQueryResult>
    {
        public string ActivationToken { get; }

        public FindUserByActivationTokenQuery(string activationToken)
        {
            ActivationToken = activationToken;
        }
    }
}
