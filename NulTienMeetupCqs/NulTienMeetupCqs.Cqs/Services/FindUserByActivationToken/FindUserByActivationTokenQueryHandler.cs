﻿using System;
using System.Linq;
using NulTienMeetupCqs.Cqs.Core.Queries;
using NulTienMeetupCqs.Cqs.Data.Repositories;

namespace NulTienMeetupCqs.Cqs.Services.FindUserByActivationToken
{
    public class FindUserByActivationTokenQueryHandler : IDomainQueryHandler<FindUserByActivationTokenQuery, FindUserByActivationTokenQueryResult>
    {
        private readonly UserRepository _userRepository;

        public FindUserByActivationTokenQueryHandler()
        {
            _userRepository = new UserRepository();
        }

        public FindUserByActivationTokenQueryResult Execute(FindUserByActivationTokenQuery query)
        {
            var userActivationExpirationPeriodInMinutes = 10000;
            var currentTimeMinusExpirationPeriod = DateTime.Now.AddMinutes(userActivationExpirationPeriodInMinutes);

            var userForActivation = _userRepository.GetAll().SingleOrDefault(user =>
                user.ActivationToken == query.ActivationToken
                && user.IsActivated == false
                && user.ActivationTokenCreatedAt != null
                && currentTimeMinusExpirationPeriod < user.ActivationTokenCreatedAt);

            return new FindUserByActivationTokenQueryResult(userForActivation); 
        }
    }
}
