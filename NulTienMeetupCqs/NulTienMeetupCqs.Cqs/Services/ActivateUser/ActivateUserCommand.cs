﻿using NulTienMeetupCqs.Cqs.Core.Commands;

namespace NulTienMeetupCqs.Cqs.Services.ActivateUser
{
  public class ActivateUserCommand : IDomainCommand
    {
        public string ActivationToken { get; }
        public string UserName { get; }
        public string Password { get; }

        public ActivateUserCommand(string activationToken, string userName, string password)
        {
            ActivationToken = activationToken;
            UserName = userName;
            Password = password;
        }
    }
}
