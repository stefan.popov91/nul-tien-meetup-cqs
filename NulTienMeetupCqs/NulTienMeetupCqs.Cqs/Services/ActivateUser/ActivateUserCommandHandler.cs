﻿using NulTienMeetupCqs.Cqs.Core.Commands;
using NulTienMeetupCqs.Cqs.Core.Queries;
using NulTienMeetupCqs.Cqs.Services.FindUserByActivationToken;
using NulTienMeetupCqs.Cqs.Validation;

namespace NulTienMeetupCqs.Cqs.Services.ActivateUser
{
    public class ActivateUserCommandHandler : IDomainCommandHandler<ActivateUserCommand>
    {
        private readonly IValidationState _validationState;
        private readonly IDomainQueryHandler<FindUserByActivationTokenQuery, FindUserByActivationTokenQueryResult> _findUserByActivationTokenQueryHandler;

        public ActivateUserCommandHandler(IValidationState validationState, IDomainQueryHandler<FindUserByActivationTokenQuery, FindUserByActivationTokenQueryResult> findUserByActivationTokenQueryHandler)
        {
            _validationState = validationState;
            _findUserByActivationTokenQueryHandler = findUserByActivationTokenQueryHandler;
        }
        public void Execute(ActivateUserCommand command)
        {
            string errorMessage = UserPasswordValidator.ValidatePassword(command.Password);

            if (!string.IsNullOrEmpty(errorMessage))
            {
                _validationState.Add(errorMessage);
                return;
            }

            var user = _findUserByActivationTokenQueryHandler.Execute(new FindUserByActivationTokenQuery(command.ActivationToken)).User;
            if (user == null || user.UserName != command.UserName)
            {
                _validationState.Add("Token does not march given userName");
                return;
            }

            user.Activate(command.Password);
        }
    }
}
