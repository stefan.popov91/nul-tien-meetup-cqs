﻿using NulTienMeetupCqs.Cqs.Core.Commands;

namespace NulTienMeetupCqs.Cqs.Services.InitiateUserActivation
{
  public  class InitiateUserActivationCommand : IDomainCommand
    {
        public int UserId { get; }

        public InitiateUserActivationCommand(int userId)
        {
            UserId = userId;
        }
    }
}
