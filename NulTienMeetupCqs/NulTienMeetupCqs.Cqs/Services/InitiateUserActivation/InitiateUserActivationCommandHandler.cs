﻿using NulTienMeetupCqs.Cqs.Core.Commands;
using NulTienMeetupCqs.Cqs.Data.Repositories;
using NulTienMeetupCqs.Cqs.Models;
using NulTienMeetupCqs.Cqs.Services.MailService;

namespace NulTienMeetupCqs.Cqs.Services.InitiateUserActivation
{
   public class InitiateUserActivationCommandHandler : IDomainCommandHandler<InitiateUserActivationCommand>
    {
        private readonly UserRepository _userRepository;
        private readonly IMailService _mailService;

        public InitiateUserActivationCommandHandler(IMailService mailService)
        {
            _userRepository = new UserRepository();
            _mailService = mailService;
        }
        public void Execute(InitiateUserActivationCommand command)
        {
            User user = _userRepository.FindById(command.UserId);
            user.SetActivationToken();
            _mailService.SendUserActivation(user);
        }
    }
}
