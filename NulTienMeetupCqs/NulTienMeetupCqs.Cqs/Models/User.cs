﻿using System;

namespace NulTienMeetupCqs.Cqs.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Telephone { get; set; }
        public bool IsActivated { get; set; }
        public Address Address { get; set; }
        public string ActivationToken { get; set; }
        public DateTime? ActivationTokenCreatedAt { get; set; }

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(UserName) &&
                   !string.IsNullOrEmpty(LastName) &&
                   !string.IsNullOrEmpty(FirstName);
        }

        public void UpdateFrom(User other)
        {
            UserName = other.UserName;
            LastName = other.LastName;
            Email = other.Email;
            Telephone = other.Telephone;
            Address = other.Address;
        }

        public void SetActivationToken()
        {
            if (IsActivated)
            {
                throw new InvalidOperationException("Activation token cannot be set for already active user");
            }
            ActivationToken = Guid.NewGuid().ToString();
            ActivationTokenCreatedAt = DateTime.Now;
        }

        public void Activate(string password)
        {
            Password = password;
            IsActivated = true;
            ActivationToken = string.Empty;
            ActivationTokenCreatedAt = null;
        }
    }
}

