﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NulTienMeetupCqs.Cqs.Validation
{
    public interface IValidationState
    {
        void Add(string errorMessage);

        void Add(ValidationResult validationResult);

        IList<ValidationResult> GetValidationResults();

        bool IsValid { get; }
    }
}
