﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NulTienMeetupCqs.Cqs.Validation
{
    public class ValidationState : IValidationState
    {
        private readonly IList<ValidationResult> _errors = new List<ValidationResult>();

        public void Add(string errorMessage)
        {
            _errors.Add(new ValidationResult(errorMessage, new List<string> { string.Empty }));
        }

        public void Add(ValidationResult validationResult)
        {
            _errors.Add(validationResult);
        }

        public IList<ValidationResult> GetValidationResults()
        {
            return _errors;
        }

        public bool IsValid => !_errors.Any();
    }
}
