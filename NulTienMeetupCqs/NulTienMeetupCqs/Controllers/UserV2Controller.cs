﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NulTienMeetupCqs.Filters;
using NulTienMeetupCqs.Models;
using NulTienMeetupCqs.Starting;
using User = NulTienMeetupCqs.Starting.User;

namespace NulTienMeetupCqs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserV2Controller : ControllerBase
    {
        private readonly UserRepository _userRepository;
        private readonly IMailService _mailService;
        private readonly IMapper _mapper;

        public UserV2Controller(IMailService mailService, IMapper mapper)
        {
            _userRepository = new UserRepository();
            _mailService = mailService;
            _mapper = mapper;
           
        }

        [HttpGet]
        [Route("V2/search/{lastName}")]
        public IList<GetUserResponse> FindAllActiveUsersByLastName(string lastName)
        {
            var result = _userRepository.GetAll().Where(u => u.LastName == lastName && u.IsActivated).ToList();
            return _mapper.Map<IList<GetUserResponse>>(result);
        }

        [HttpPost]      
        [Route("V2")]
        [TransactionFilter]
        public void AddUser(UpdateUserRequest userRequest)
        {
            var user = _mapper.Map(userRequest, new User());
            if (UserWithSameUserNameAlreadyExists(user))
                return;

            if (!IsUserValid(user))
                return;

            _userRepository.Add(user);
        }

        private bool UserWithSameUserNameAlreadyExists(User user)
        {
            var existingUser = _userRepository.GetAll().FirstOrDefault(u => u.UserName == user.UserName);
            return existingUser != null;
        }

        [HttpPut]
        [Route("V2/{userId}")]
        [TransactionFilter]
        public void UpdateUser(int userId, UpdateUserRequest userRequest)
        {
            var user = _mapper.Map(userRequest, new User());
            if (!IsUserValid(user))
                return;

            var existingUser = _userRepository.FindById(user.Id);
            existingUser.UserName = user.UserName;
            existingUser.LastName = user.LastName;
            existingUser.Email = user.Email;
            existingUser.Telephone = user.Telephone;
            existingUser.Address = user.Address;
        }

        private bool IsUserValid(User user)
        {
            return !string.IsNullOrEmpty(user.UserName) &&
                   !string.IsNullOrEmpty(user.LastName) &&
                   !string.IsNullOrEmpty(user.FirstName);
        }


        [HttpPost]
        [Route("V2/{userId}/token")]
        public void InitiateUserActivation(int userId)
        {
            User user = _userRepository.FindById(userId);
            user.ActivationToken = Guid.NewGuid().ToString();
            user.ActivationTokenCreatedAt = DateTime.Now;
            _mailService.SendUserActivation(user);
        }

        [HttpPut]
        [Route("V2/activate")]
        [TransactionFilter]
        public string ActivateUser([FromBody] ActivateUserRequest activateUserRequest)
        {
            string errorMessage = UserPasswordValidator.ValidatePassword(activateUserRequest.Password);
            if (!string.IsNullOrEmpty(errorMessage))
                return errorMessage;

            User user = FindByActivationToken(activateUserRequest.ActivationToken);
            if (user == null || user.UserName != activateUserRequest.UserName)
                return "Token does not march given userName";

            Activate(user, activateUserRequest.Password);
            return null;
        }

        private User FindByActivationToken(string token)
        {
            var userActivationExpirationPeriodInMinutes = 10000;
            var currentTimeMinusExpirationPeriod = DateTime.Now.AddMinutes(userActivationExpirationPeriodInMinutes);

            return _userRepository.GetAll().SingleOrDefault(user =>
                user.ActivationToken == token
                && user.IsActivated == false
                && user.ActivationTokenCreatedAt != null
                && currentTimeMinusExpirationPeriod < user.ActivationTokenCreatedAt);
        }

        private void Activate(User user, string password)
        {
            user.Password = password;
            user.IsActivated = true;
            user.ActivationToken = string.Empty;
            user.ActivationTokenCreatedAt = null;
        }
    }
}
