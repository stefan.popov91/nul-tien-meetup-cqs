﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NulTienMeetupCqs.Cqs.Core.Commands;
using NulTienMeetupCqs.Cqs.Core.Queries;
using NulTienMeetupCqs.Cqs.Models;
using NulTienMeetupCqs.Cqs.Services.ActivateUser;
using NulTienMeetupCqs.Cqs.Services.AddUser;
using NulTienMeetupCqs.Cqs.Services.FindAllUsersByLastName;
using NulTienMeetupCqs.Cqs.Services.InitiateUserActivation;
using NulTienMeetupCqs.Cqs.Services.UpdateUser;
using NulTienMeetupCqs.Filters;
using NulTienMeetupCqs.Models;

namespace NulTienMeetupCqs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IDomainQueryHandler<FindAllActiveUsersByLastNameQuery, FindAllActiveUsersByLastNameQueryResult> _findAllActiveUsersByLastName;
        private readonly IDomainCommandHandler<AddUserCommand> _addUserCommandHandler;
        private readonly IDomainCommandHandler<UpdateUserCommand> _updateUserCommandHandler;
        private readonly IDomainCommandHandler<InitiateUserActivationCommand> _initiateUserActivationCommandHandler;
        private readonly IDomainCommandHandler<ActivateUserCommand> _activateUserCommandHandler;

        public UserController(IMapper mapper, 
            IDomainQueryHandler<FindAllActiveUsersByLastNameQuery, FindAllActiveUsersByLastNameQueryResult> findAllActiveUsersByLastName,
            IDomainCommandHandler<AddUserCommand> addUserCommandHandler,
            IDomainCommandHandler<UpdateUserCommand> updateUserCommandHandler,
            IDomainCommandHandler<InitiateUserActivationCommand> initiateUserActivationCommandHandler,
            IDomainCommandHandler<ActivateUserCommand> activateUserCommandHandler)
        {
            _mapper = mapper;
            _findAllActiveUsersByLastName = findAllActiveUsersByLastName;
            _addUserCommandHandler = addUserCommandHandler;
            _updateUserCommandHandler = updateUserCommandHandler;
            _initiateUserActivationCommandHandler = initiateUserActivationCommandHandler;
            _activateUserCommandHandler = activateUserCommandHandler;
        }

        [HttpGet]
        [Route("search/{lastName}")]
        public IList<GetUserResponse> FindAllActiveUsersByLastName(string lastName)
        {
            var result = _findAllActiveUsersByLastName.Execute(new FindAllActiveUsersByLastNameQuery(lastName));
            return _mapper.Map<IList<GetUserResponse>>(result.Users);
        }

        [HttpPost]      
        [Route("")]
        [TransactionFilter]
        public void AddUser(UpdateUserRequest userRequest)
        {
           _addUserCommandHandler.Execute(new AddUserCommand(_mapper.Map(userRequest, new User())));
        }

        [HttpPut]
        [Route("{userId}")]
        [TransactionFilter]
        public void UpdateUser(int userId, UpdateUserRequest userRequest)
        {
            _updateUserCommandHandler.Execute(new UpdateUserCommand(_mapper.Map<User>(userRequest)));
        }

        [HttpPost]
        [Route("{userId}/token")]
        public void InitiateUserActivation(int userId)
        {
            _initiateUserActivationCommandHandler.Execute(new InitiateUserActivationCommand(userId));
        }

        [HttpPut]
        [Route("activate")]
        [TransactionFilter]
        public void ActivateUser([FromBody] ActivateUserRequest activateUserRequest)
        {
            _activateUserCommandHandler.Execute(new ActivateUserCommand(activateUserRequest.ActivationToken, activateUserRequest.UserName, activateUserRequest.Password));
        }
    }
}
