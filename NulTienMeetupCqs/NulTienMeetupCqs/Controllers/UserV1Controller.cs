﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NulTienMeetupCqs.Filters;
using NulTienMeetupCqs.Models;
using NulTienMeetupCqs.Starting;
using User = NulTienMeetupCqs.Starting.User;

namespace NulTienMeetupCqs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserV1Controller : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public UserV1Controller(IMapper mapper, 
          IUserService userService)
        {
            _mapper = mapper;
            _userService = userService;
        }

        [HttpGet]
        [Route("V1/search/{lastName}")]
        public IList<GetUserResponse> FindAllActiveUsersByLastName(string lastName)
        {
            var result = _userService.FindAllActiveUsersByLastName(lastName);
            return _mapper.Map<IList<GetUserResponse>>(result);
        }

        [HttpPost]      
        [Route("V1")]
        [TransactionFilter]
        public void AddUser(UpdateUserRequest userRequest)
        {
            _userService.AddUser(_mapper.Map(userRequest, new User()));
        }

        [HttpPut]
        [Route("V1/{userId}")]
        [TransactionFilter]
        public void UpdateUser(int userId, UpdateUserRequest userRequest)
        {
            _userService.UpdateUser(_mapper.Map(userRequest, new User()));
        }

        [HttpPost]
        [Route("V1/{userId}/token")]
        public void InitiateUserActivation(int userId)
        {
            _userService.InitiateUserActivation(userId);
        }

        [HttpPut]
        [Route("V1/activate")]
        [TransactionFilter]
        public void ActivateUser([FromBody] ActivateUserRequest activateUserRequest)
        {
            _userService.ActivateUser(activateUserRequest.ActivationToken, activateUserRequest.UserName,
                activateUserRequest.Password);
        }
    }
}
