﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using NulTienMeetupCqs.Core;
using NulTienMeetupCqs.Cqs.Validation;

namespace NulTienMeetupCqs.Filters
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class TransactionFilterAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext context)
        {
            IUnitOfWork unitOfWork = context.HttpContext.RequestServices.GetService<IUnitOfWork>();
            IValidationState validationState = context.HttpContext.RequestServices.GetService<IValidationState>();

            if (context.Exception == null &&
                context.HttpContext.Response.StatusCode >= 200 &&
                context.HttpContext.Response.StatusCode < 300)
            {
                if (validationState.IsValid && context.ModelState.IsValid)
                {
                    unitOfWork.SaveChanges();
                }
            }
        }
    }
}
