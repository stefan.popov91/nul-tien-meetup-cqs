﻿using NulTienMeetupCqs.Cqs.Models;

namespace NulTienMeetupCqs.Models
{
    public class GetUserResponse
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Telephone { get; set; }

        public bool IsActivated { get; set; }

        public Address Address { get; set; }
    }
}
