﻿namespace NulTienMeetupCqs.Models
{
    public class ActivateUserRequest
    {
        public string ActivationToken { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
