﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using NulTienMeetupCqs.Mediator.Orders.Commands;

namespace NulTienMeetupCqs.Configuration
{
    public static class MediatRConfigurationExtensions
    {
        public static void ConfigureMediatR(this IServiceCollection services)
        {
            //services.AddScoped(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>));
            //services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddMediatR(
                typeof(AddOrderCommand).Assembly
            );
        }
    }
}
