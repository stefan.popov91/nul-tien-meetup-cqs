﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace NulTienMeetupCqs.Core
{
   public class DbContextUnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;
        private readonly ILogger _logger;
        private bool _cancelSaving;

        public DbContextUnitOfWork(DbContext context, ILogger<DbContextUnitOfWork> logger)
        {
            _context = context;
            _logger = logger;
        }

        public void SaveChanges()
        {
            if (_cancelSaving)
            {
                _logger.LogWarning("Not saving database changes since saving was cancelled.");
                return;
            }

            int numberOfChanges = _context.SaveChanges();

            _logger.LogDebug($"{numberOfChanges} of changed were saved to database");
        }

        public void CancelSaving()
        {
            _cancelSaving = true;
        }
    }
}