﻿namespace NulTienMeetupCqs.Core
{
  public interface IUnitOfWork
    {
        void SaveChanges();
        void CancelSaving();
    }
}
