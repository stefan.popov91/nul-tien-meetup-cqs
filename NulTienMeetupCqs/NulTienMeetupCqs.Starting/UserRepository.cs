﻿using System.Collections.Generic;
using System.Linq;

namespace NulTienMeetupCqs.Starting
{
    public class UserRepository
    {
        private readonly IList<User> _users;

        public UserRepository()
        {
            _users = new List<User>
            {
                new User
                {
                    Id = 1, FirstName = "Petar", LastName = "Petrovic", Email = "patar.petrovic@gmail.com",
                    UserName = "ppetrovic", Password = "Pass123!", Telephone = "3815477789",
                    Address = new Address
                    {
                        Street = "Sterijina", HouseNumber = "11", HouseNumberAddition = "a", PostalCode = "11000",
                        City = "Beograd", Country = "Srbija"
                    }
                },
                new User
                {
                    Id = 2, FirstName = "Milica", LastName = "Maric", Email = "milica.maric@gmail.com",
                    UserName = "mmaric", Password = "sPass1343!", Telephone = "38156484666",
                    Address = new Address
                    {
                        Street = "Beogradska", HouseNumber = "89", PostalCode = "11000",
                        City = "Beograd", Country = "Srbija"
                    }
                },
                new User
                {
                    Id = 3, FirstName = "Ana", LastName = "Lazic", Email = "ana.lazic@gmail.com",
                    UserName = "alazic", Password = "$hgbdas", Telephone = "38168745664",
                    Address = new Address
                    {
                        Street = "Cara Dusana", HouseNumber = "15", PostalCode = "11000",
                        City = "Beograd", Country = "Srbija"
                    }
                }
            };
        }

        public User FindById(int id)
        {
            return _users.FirstOrDefault(u => u.Id == id);
        }

        public IList<User> GetAll()
        {
            return _users;
        }
        
        public void Add(User user)
        {
            _users.Add(user);
        }
    }
}