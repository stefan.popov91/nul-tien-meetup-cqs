﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NulTienMeetupCqs.Starting
{
    public class UserService: IUserService
    {
        private readonly IMailService _mailService;
        private readonly UserRepository _userRepository;

        public UserService(IMailService mailService)
        {
            _mailService = mailService;
            _userRepository = new UserRepository();
        }

        public IList<User> FindAllActiveUsersByLastName(string lastName)
        {
            return _userRepository.GetAll().Where(u => u.LastName == lastName && u.IsActivated).ToList();
        }

        public void AddUser(User user)
        {
            if (UserWithSameUserNameAlreadyExists(user))
                return;

            if (!IsUserValid(user))
                return;

            _userRepository.Add(user);
        }
        
        private bool UserWithSameUserNameAlreadyExists(User user)
        {
            var existingUser = _userRepository.GetAll().FirstOrDefault(u => u.UserName == user.UserName);
            return existingUser != null;
        }

        public void UpdateUser(User user)
        {
            if (!IsUserValid(user))
                return;

            var existingUser = _userRepository.FindById(user.Id);
            existingUser.UserName = user.UserName;
            existingUser.LastName = user.LastName;
            existingUser.Email = user.Email;
            existingUser.Telephone = user.Telephone;
            existingUser.Address = user.Address;
        }

        private bool IsUserValid(User user)
        {
            return !string.IsNullOrEmpty(user.UserName) &&
                   !string.IsNullOrEmpty(user.LastName) &&
                   !string.IsNullOrEmpty(user.FirstName);
        }

        public void InitiateUserActivation(int userId)
        {
            User user = _userRepository.FindById(userId);
            if (user.IsActivated)
            {
                throw new InvalidOperationException("Activation token cannot be set for already active user");
            }

            user.ActivationToken = Guid.NewGuid().ToString();
            user.ActivationTokenCreatedAt = DateTime.Now; 
            _mailService.SendUserActivation(user);
        }

        public string ActivateUser(string activationToken, string userName, string password)
        {
            string errorMessage = UserPasswordValidator.ValidatePassword(password);
            if (!string.IsNullOrEmpty(errorMessage))
                return errorMessage;

            User user = FindByActivationToken(activationToken);
            if (user == null || user.UserName != userName)
                return "Token does not march given userName";

            Activate(user, password);
            return null;
        }

        private User FindByActivationToken(string token)
        {
            var userActivationExpirationPeriodInMinutes = 10000;
            var currentTimeMinusExpirationPeriod = DateTime.Now.AddMinutes(userActivationExpirationPeriodInMinutes);

            return _userRepository.GetAll().SingleOrDefault(user =>
                user.ActivationToken == token
                && user.IsActivated == false
                && user.ActivationTokenCreatedAt != null
                && currentTimeMinusExpirationPeriod < user.ActivationTokenCreatedAt);
        }

        private void Activate(User user, string password)
        {
            user.Password = password;
            user.IsActivated = true;
            user.ActivationToken = string.Empty;
            user.ActivationTokenCreatedAt = null;
        }
    }
}
