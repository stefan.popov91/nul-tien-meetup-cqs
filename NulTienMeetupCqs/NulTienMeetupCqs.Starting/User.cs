﻿using System;

namespace NulTienMeetupCqs.Starting
{
   public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Telephone { get; set; }
        public Address Address { get; set; }
        public bool IsActivated { get; set; }
        public string ActivationToken { get; set; }
        public DateTime? ActivationTokenCreatedAt { get; set; }
    }
}
