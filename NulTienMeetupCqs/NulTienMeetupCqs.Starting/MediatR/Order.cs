﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NulTienMeetupCqs.Starting.MediatR
{
    public class Order
    {
        public int Id { get; set; }
        public int TotalPrice { get; set; }
        //public int TotalPrice => OrderItems.Sum(x => x.Amount * x.Item.Price);
        public IEnumerable<OrderItem> OrderItems { get; set; }
        public bool Charged { get; set; }

        
    }
}
