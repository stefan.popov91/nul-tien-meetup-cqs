﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NulTienMeetupCqs.Starting.MediatR
{
    public class OrderItem
    {
        public Item Item { get; set; }
        public int Amount { get; set; }
    }
}
