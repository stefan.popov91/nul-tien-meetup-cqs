﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NulTienMeetupCqs.Starting.MediatR
{
    public class WaiterService
    {
        private readonly Repositories _repositories;
        public WaiterService()
        {
            _repositories = new Repositories();
        }

        public void AddOrUpdateOrder(AddOrUpdateOrderRequest request)
        {
            if (request.Id.HasValue)
            {
                var order = _repositories.Orders.Single(x => x.Id == request.Id);
                order.OrderItems.ToList().AddRange(request.OrderItems);
                order.TotalPrice += GetTotalPrice(request.OrderItems);
            }
            else
            {
                var order = new Order
                {
                    Id = _repositories.Orders.Max(x => x.Id) + 1,
                    OrderItems = request.OrderItems,
                    TotalPrice = GetTotalPrice(request.OrderItems.ToList())
                };
                _repositories.Orders.Add(order);
            }
        }

        public int ChargeOrder(int orderId, int paidAmount)
        {
            var order = _repositories.Orders.Single(x => x.Id == orderId);
            if (paidAmount < order.TotalPrice)
            {
                throw new Exception("You don't have enough money");
            }

            order.Charged = true;
            return paidAmount - order.TotalPrice;
        }

        public List<Order> GetActiveOrders()
        {
            return _repositories.Orders.Where(x => x.Charged == false).ToList();
        }

        private int GetTotalPrice(IList<OrderItem> items)
        {
            return items.Sum(x => x.Item.Price * x.Amount);
        }
    }
}
