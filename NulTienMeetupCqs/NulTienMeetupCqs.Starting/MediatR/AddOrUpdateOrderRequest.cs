﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NulTienMeetupCqs.Starting.MediatR
{
    public class AddOrUpdateOrderRequest
    {
        public int? Id { get; set; }
        public IList<OrderItem> OrderItems { get; set; }
    }
}
