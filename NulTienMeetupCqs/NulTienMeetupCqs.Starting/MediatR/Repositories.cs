﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NulTienMeetupCqs.Starting.MediatR
{
    public class Repositories
    {
        public Repositories()
        {
            if(Orders == null) Orders = new List<Order>();
        }
        public IList<Order> Orders { get; }
    }
}
