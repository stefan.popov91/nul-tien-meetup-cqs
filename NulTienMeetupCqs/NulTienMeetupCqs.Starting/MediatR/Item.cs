﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NulTienMeetupCqs.Starting.MediatR
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
    }
}
