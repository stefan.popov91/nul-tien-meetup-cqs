﻿using System.Text.RegularExpressions;

namespace NulTienMeetupCqs.Starting
{
    public static class UserPasswordValidator
    {
        public static string ValidatePassword(string password)
        {
            bool isValid = password.Length > 8 && AlphabeticLowercaseLetterPasswordValidation(password) &&
                           AlphabeticUppercaseLetterPasswordValidation(password) && NumericPasswordValidation(password) &&
                           NonAlphaNumericValidation(password);

            if (!isValid)
            {
                return "Het wachtwoord moet voldoen aan de volgende eisen: \n" +
                       "-Een minimale lengte van 9 karakters hebben; \n " +
                       "-Minimaal 1 numeriek (1 t/ m 9), \n" +
                       " 2 alfanumerieke (waarvan 1 hoofdletter (A t/m Z) en 1 kleinletter (a t/m z)) \n" +
                       " en 1 non- (alfa)numeriek (b.v. !@#$ %^&*?) karakter hebben.";
            }

            return null;
        }

        private static bool AlphabeticLowercaseLetterPasswordValidation(string password)
        {
            return Regex.IsMatch(password, @"[a-z]+");
        }

        private static bool AlphabeticUppercaseLetterPasswordValidation(string password)
        {
            return Regex.IsMatch(password, @"[A-Z]+");
        }

        private static bool NumericPasswordValidation(string password)
        {
            return Regex.IsMatch(password, @"[0-9]");
        }

        private static bool NonAlphaNumericValidation(string password)
        {
            return Regex.IsMatch(password, @"[^a-zA-Z0-9 -]");
        }
    }
}