﻿namespace NulTienMeetupCqs.Starting
{
    public interface IMailService
    {
        void SendUserActivation(User user);
    }
}
