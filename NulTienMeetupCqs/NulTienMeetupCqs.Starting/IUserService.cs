﻿using System.Collections.Generic;

namespace NulTienMeetupCqs.Starting
{
    public interface IUserService
    {
        IList<User> FindAllActiveUsersByLastName(string lastName);

        void AddUser(User user);

        void UpdateUser(User user);

        void InitiateUserActivation(int userId);

        string ActivateUser(string activationToken, string userName, string password);
    }
}
