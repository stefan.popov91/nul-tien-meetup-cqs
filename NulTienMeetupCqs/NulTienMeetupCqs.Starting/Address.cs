﻿namespace NulTienMeetupCqs.Starting
{
  public class Address
    {
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string HouseNumberAddition { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
